@extends('acctype.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div>
                        <a class="btn btn-primary" href="{{ route('acctype.create') }}">เพิ่ม</a>
                    </div>
                    <table border=1>
                        <tr align="center">
                            <td>ชนิดบัญชี</td>
                            <td>ชื่อประเภท</td>
                            <td>วันที่เริ่มต้น</td>
                            <td>วันที่สิ้นสุด</td>
                            <td>ดอกเบี้ย</td>
                            <td colspan=2>การดำเนินงาน</td>
                        </tr>
                        @foreach($acctype as $acct)
                        <tr>
                            <td align="center">{{ $acct->Type_No }}</td>
                            <td>{{ $acct->Type_Name }}</td>
                            <td>{{ $acct->DateBegin }}</td>
                            <td>{{ $acct->DateEnd }}</td>
                            <td align="center">{{ $acct->Rate }}</td>
                            <td>
                                Edit
                                Delete
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection