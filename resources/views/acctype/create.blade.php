@extends('acctype.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div>
                        ประเภทบัญชี
                    </div>
                    <form action="{{ route('acctype.store') }}" method="POST">
                        @csrf
                        @method("POST")
                        <table border=1>
                            <tr>
                                <td>ชนิดบัญชี</td>
                                <td><input type="text" name="Type_No"></td>
                            </tr>
                            <tr>
                                <td>ชื่อประเภท</td>
                                <td><input type="text" name="Type_Name"></td>
                            </tr>
                            <tr>
                                <td>วันที่เริ่มต้น</td>
                                <td><input type="text" name="DateBegin"></td>
                            </tr>
                            <tr>
                                <td>วันที่สิ้นสุด</td>
                                <td><input type="text" name="DateEnd"></td>
                            </tr>
                            <tr>
                                <td>ดอกเบี้ย</td>
                                <td><input type="text" name="Rate"></td>
                            </tr>
                            <tr>
                                <td colspan=2>
                                    <button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
                                    <button type="reset" class="btn btn-danger">ยกเลิก</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> @endsection