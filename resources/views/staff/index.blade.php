@extends('staff.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div>
                        <a class="btn btn-primary" href="{{ route('staff.create') }}">เพิ่ม</a>
                        <!-- <a class="btn btn-success" href=" ">โอนเงิน</a>
                        <a class="btn btn-danger" href="{{ route('account.calculaterate') }}">คำนวณดอกเบี้ย</a> -->
                    </div>
                    <table border=1>
                        <tr>
                            <td>เลขประจำตัว</td>
                            <td>ชื่อ</td>
                            <td>นามสกุล</td>
                            <td>ที่อยู่</td>
                            <td>อีเมล์</td>
                            <td>เบอร์โทรศัพท์</td>
                            <td colspan=2>การดำเนินงาน</td>
                        </tr>
                        @foreach($staff as $sta)
                        <tr>
                            <td>{{ $sta->Staff_No }}</td>
                            <td>{{ $sta->Staff_Name }}</td>
                            <td>{{ $sta->Staff_Surname }}</td>
                            <td>{{ $sta->Staff_Address }}</td>
                            <td>{{ $sta->Staff_Email }}</td>
                            <td>{{ $sta->Staff_Phone }}</td>
                            <td>
                                <form action="{{ route('staff.destroy',$sta->Staff_No) }}" method="POST">
                                    <a class="btn btn-primary" href="{{ route('staff.edit',$sta->Staff_No) }}">Edit</a>
                                    @csrf
                                    @method("DELETE")
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection