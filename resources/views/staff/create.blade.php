@extends('staff.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div>
                        ADD STAFF
                    </div>
                    <form action="{{ route('staff.store') }}" method="POST">
                        @csrf
                        @method("POST")
                        <table border="1">
                            <tr>
                                <td>เลขประจำตัว</td>
                                <td><input type="text" name="Staff_No"></td>
                            </tr>
                            <tr>
                                <td>ชื่อ</td>
                                <td><input type="text" name="Staff_Name"></td>
                            </tr>
                            <tr>
                                <td>นามสกุล</td>
                                <td><input type="text" name="Staff_Surname"></td>
                            </tr>
                            <tr>
                                <td>ที่อยู่</td>
                                <td><input type="text" name="Staff_Address"></td>
                            </tr>
                            <tr>
                                <td>อีเมล์</td>
                                <td><input type="text" name="Staff_Email"></td>
                            </tr>
                            <tr>
                                <td>เบอร์โทรศัพท์</td>
                                <td><input type="text" name="Staff_Phone"></td>
                            </tr>

                            <tr>
                                <td colspan=2>
                                    <button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
                                    <button type="reset" class="btn btn-danger">ยกเลิก</button>
                                </td>
                            </tr>

                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection