<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ACCTypeModel extends Model
{
    protected $table="Staff";
    protected $fillable=[
        'Staff_No','Staff_Name','Staff_Surname','Staff_Address','Staff_Email','Staff_Phone'
    ];
}
