<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // select * from Account
        // $account = DB::table('Account')->get();
        // return view('account.index', compact('account'));
        $staff = DB::table('Staff')->get();
        return view('staff.index', compact('staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'Staff_No' => 'required',
            'Staff_Name' => 'required',
            'Staff_Surname' => 'required',
            'Staff_Address' => 'required',
            'Staff_Email' => 'required',
            'Staff_Phone' => 'required'
            
        ]);

        DB::table('Staff')->insert([

            'Staff_No' => $request->Staff_No,
            'Staff_Name' => $request->Staff_Name,
            'Staff_Surname' => $request->Staff_Surname,
            'Staff_Address' => $request->Staff_Address,
            'Staff_Email' => $request->Staff_Email,
            'Staff_Phone' => $request->Staff_Phone
           

        ]);
        return redirect('staff/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function CalculateRate()
    {
        // $accounts = DB::table('Account')->get();
        // foreach ($accounts as $account)
        // {
        //     DB::select('call CalInterestUPD(?)',array($account->ACC_No));
        // }
        // redirect('account');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $account = DB::table('Account')->where('ACC_No','=',$id)->get();
        // return view('account.edit',compact('account'));
        $staff = DB::table('Staff')->where('Staff_No','=',$id)->get();
        return view('staff.edit',compact('staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'Staff_No' => 'required',
            'Staff_Name' => 'required',
            'Staff_Surname' => 'required',
            'Staff_Address' => 'required',
            'Staff_Email' => 'required',
            'Staff_Phone' => 'required'
            
        ]);

        DB::table('Staff')->where('Staff_No','=',$id)->update([

            'Staff_No' => $request->Staff_No,
            'Staff_Name' => $request->Staff_Name,
            'Staff_Surname' => $request->Staff_Surname,
            'Staff_Address' => $request->Staff_Address,
            'Staff_Email' => $request->Staff_Email,
            'Staff_Phone' => $request->Staff_Phone
        ]);
        
        return redirect('staff');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // DB::table("Account")->where('ACC_No','=',$id)->delete();
        // return redirect('account');
        DB::table("Staff")->where('Staff_No','=',$id)->delete();
        return redirect('staff');
    }
}
